# step installazione

## installare now-cli

e' l'eseguibile che permette di deployare app su zeit. puoi seguire le istruzioni qui https://zeit.co/docs

in breve: se hai installato nodejs, devi fare:

	npm i -g now


## login 

dopo averlo installato ti devi creare un account su zeit.co e fare il login con lo script

	now login


## deploy 

a login effettuato, all'interno della directory che contiene l'applicazione ti basta lanciare

	now

ogni volta che vuoi effetturare il deploy (ogni volta che aggiorni l'api)


alla fine del deploy vedrai una url simile, che ti permette di accedere all'api

https://mlol-video.raffaele.now.sh/filmId/a5dbe979-3200-11e9-8fee-002481fac568