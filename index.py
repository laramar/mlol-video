import flask
from flask import request, jsonify, render_template
import requests
import re
from bs4 import BeautifulSoup as bs4
import time
import json
import unidecode


app = flask.Flask(__name__)
#app.config["DEBUG"] = True
	
	
@app.route('/filmId/<filmid>', methods=['GET'])

def filmId(filmid): 
	retrieve_url = "https://medialibrary.streamup.eu/api/videos/video/index.php?videoId="+filmid
	response = requests.get(retrieve_url)
	key = response.json()
	videoId = key[0]["videoId"]
	title = key[0]["title"].strip().lower()
	director = re.sub(r'\\.+', '', key[0]["regia"]).strip()

	#nowWikidata
	url = 'https://query.wikidata.org/sparql'
	query = """
	SELECT ?film ?filmLabel ?directorLabel WHERE {
	  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],it". }
	  OPTIONAL {  }
	  ?film rdfs:label ?filmLabel. 
      filter contains(lcase(?filmLabel), "%s"@it). 
	  ?director ?label "%s"@it.
	  ?film wdt:P57 ?director.
	}
	""" % (title, director)
	user_agent = {'User-agent':"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11"}
	r = requests.get(url, params = {'format': 'xml', 'query': query})
	content = r.text
	soup = bs4(content, "lxml")
	try:
		uri = soup.find("uri").text.strip().replace("http://www.wikidata.org/entity/", "")
	except:
		uri = None
	if uri is not None:
		get_wikipedia = 'https://hub.toolforge.org/'+uri+"?format=json&lang=it"
		retrieve_wikipedia = requests.get(get_wikipedia)
		wikipedia_data = retrieve_wikipedia.json()
		wikipedia_url = wikipedia_data["destination"]["url"]
		wikipedia_id = wikipedia_url.replace("https://it.wikipedia.org/wiki/", "")
		retrieve_wikipedia2 = requests.get("https://it.wikipedia.org/w/api.php?action=query&format=json&titles=%s&prop=extracts&exsentences=10&explaintext" % wikipedia_id)
		print(retrieve_wikipedia2)
		wikipedia_data2 = retrieve_wikipedia2.json()
		pages = wikipedia_data2['query']['pages'].values()
		sinossi = list(pages)[0]['extract'].replace("\n\n== Trama ==\n", "").replace("\n", "<br />")
		if "== "in sinossi:
			sinossi = re.sub(r'==.+', "", sinossi)
		else:
			pass
		fonte = "Wikipedia"
		fonte_url = wikipedia_url
	else:
		#nowTMDB
		direct_list = []
		id_movie = []
		retrieve_tmdb1 =  "https://api.themoviedb.org/3/search/movie?api_key=501d98ec2896d8a2a92c932104010b7a&language=it-IT&query=%s&page=1&include_adult=false" % title
		print(retrieve_tmdb1)
		response_tmdb = requests.get(retrieve_tmdb1)
		tmdb_data = response_tmdb.json()
		for r in tmdb_data["results"]:
			id_movie.append(r["id"])
		for i in id_movie:
			second_url = "https://api.themoviedb.org/3/movie/%s?api_key=501d98ec2896d8a2a92c932104010b7a&language=it-IT&append_to_response=credits" % i #compose the api to get the movie details from the tmdb id
			print(second_url)
			resp = requests.get(second_url)
			tmdb_data2 = resp.json()
			for c in tmdb_data2["credits"]["crew"]: 
				if c["job"] == "Director": #search for the director job
					director_tmdb = direct_list.append(unidecode.unidecode(c["name"])) #and create the list of directors to search in
					#THE BIG IF
					if director in direct_list:
						sinossi = tmdb_data2["overview"]
						fonte = "TMDB"
						fonte_url = "https://www.themoviedb.org/movie/"+str(i)
					else:
						pass
				else:
					sinossi = ""
					fonte = ""
					fonte_url = ""

		
	return jsonify(videoId= videoId, title= title, director = director, sinossi = sinossi, fonte = fonte, fonte_url = fonte_url)
if __name__ == '__main__':
	app.run(debug=True)
